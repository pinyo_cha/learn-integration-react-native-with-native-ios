//
//  PhotoAlbumReactNativeViewController.swift
//  LearnIntegrationReactNative
//
//  Created by Pinyo.Cha on 26/3/2562 BE.
//  Copyright © 2562 Pinyo.Cha. All rights reserved.
//

import Foundation
import UIKit
import React
import SnapKit

class PhotoAlbumRCTViewController: UIViewController {
    var reactView: RCTRootView!
    var itemId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        guard let id = itemId else { return }
        let mockData = ["title1": "Developer", "title2": "Cybertron", "title3": "The Pinyo."]
        
        let moduleView = ReactBridge.shared.viewForModule("LearnReactNative", initialProperties: mockData)
        
        moduleView.backgroundColor = UIColor.cyan
        view.addSubview(moduleView)
        
        moduleView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self.view)
        }
    }
}
