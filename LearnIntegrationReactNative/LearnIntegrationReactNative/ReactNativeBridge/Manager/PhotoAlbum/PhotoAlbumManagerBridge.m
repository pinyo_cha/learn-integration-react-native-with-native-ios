//
//  PhotoAlbumManagerBridge.h
//  LearnIntegrationReactNative
//
//  Created by Pinyo.Cha on 27/3/2562 BE.
//  Copyright © 2562 Pinyo.Cha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(PhotoAlbumManager, NSObject)

RCT_EXTERN_METHOD(presentSomething:(nonnull NSNumber *)reactTag)
RCT_EXTERN_METHOD(openFacebookWebView)
@end
