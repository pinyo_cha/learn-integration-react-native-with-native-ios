//
//  photoAlbumManager.swift
//  LearnIntegrationReactNative
//
//  Created by Pinyo.Cha on 27/3/2562 BE.
//  Copyright © 2562 Pinyo.Cha. All rights reserved.
//

import Foundation
import React

@objc(PhotoAlbumManager)
class PhotoAlbumManager: NSObject {
    var bridge: RCTBridge!
    
    @objc func dismissPresentedViewController(_ reactTag: NSNumber) {
        print("[RN Call this method: \(reactTag)")
        DispatchQueue.main.async {
            print("[RN] Do something")
//            guard let view = self.bridge.uiManager.view(forReactTag: reactTag) else { return }
//            let presentedVC = view.reactViewController()
//            presentedVC?.dismiss(animated: true, completion: nil)
        }
    }
}

extension PhotoAlbumManager {
    @objc func constantsToExport() -> [String: Any]! {
        return ["userId": "TMN-191",
                "userType": "Member VIP"]
    }
}

extension PhotoAlbumManager {
    
    @objc(openFacebookWebView)
    func openFacebookWebView() -> Void {
        print("[RN] call open facebok webview")
    }
}
