//
//  ReactBridgeModule.swift
//  LearnIntegrationReactNative
//
//  Created by Pinyo.Cha on 26/3/2562 BE.
//  Copyright © 2562 Pinyo.Cha. All rights reserved.
//

import Foundation
import React

let ip = "10.164.12.24"
let localUrl = "http://" + ip + ":8081/index.bundle?platform=ios&dev=true"

let enableServer = false

class ReactBridge: NSObject {
    static let shared = ReactBridge()
}

extension ReactBridge: RCTBridgeDelegate {
    func sourceURL(for bridge: RCTBridge!) -> URL! {
        return URL(string: localUrl)
    }
}

extension ReactBridge {
    func createBridgeIfNeed() -> RCTBridge {
        let bridge = RCTBridge.init(delegate: self, launchOptions: nil)
        return bridge ?? RCTBridge()
    }
    
    func viewForModule(_ moduleName: String, initialProperties: [String: Any]?) -> RCTRootView {
        if enableServer {
            let viewBridge = createBridgeIfNeed()
            guard let rootView: RCTRootView = RCTRootView(bridge: viewBridge, moduleName: moduleName, initialProperties: initialProperties) else { return RCTRootView() }
            return rootView
        }
        else {
            if let iosBundle = Bundle.main.url(forResource: "main", withExtension: "jsbundle") {
                guard let bundleRootView = RCTRootView(bundleURL: iosBundle, moduleName: moduleName, initialProperties: initialProperties, launchOptions: nil) else { return RCTRootView() }
                return bundleRootView
            }
            
        }
        return RCTRootView()
    }
}
