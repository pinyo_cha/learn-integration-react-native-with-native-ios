//
//  ViewController.swift
//  LearnIntegrationReactNative
//
//  Created by Pinyo.Cha on 26/3/2562 BE.
//  Copyright © 2562 Pinyo.Cha. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func onPressButton(_ sender: UIButton) {
        let view = PhotoAlbumRCTViewController()
        navigationController?.pushViewController(view, animated: true)
    }
}

